-- criar outro banco pra rodas os testes

CREATE TABLE Cliente(
	codcliente SERIAL,
	cpf NUMERIC(5,0),
	nome VARCHAR(45),
	sexo CHAR(1),
	data_nascimento DATE,
	logradouro VARCHAR(45),
	numero SMALLINT,
	complemento VARCHAR(30),
	bairro VARCHAR(30),
	cidade VARCHAR(30),
	estado VARCHAR(15),
	cep CHAR(8),
	
	PRIMARY KEY (codcliente),
	(codcliente,cpf,nome,sexo,data_nascimento,logradouro,numero,bairro) IS NOT NULL,
	CONSTRAINT sexo_valido CHECK(sexo in ('m','f','o')),

);

CREATE TABLE Marca(
	codmarca SERIAL,
	marca VARCHAR(15),
	
	PRIMARY KEY (codmarca)
);

CREATE TABLE Carro(
	codcarro SERIAL,
	modelo VARCHAR(20),
	placa CHAR(8),
	marca INT,
	
	PRIMARY KEY (codcarro),
	FOREIGN KEY (marca) REFERENCES Marca(codmarca),
	CONSTRAINT placa_valida CHECK (placa ~* '([A-Z]{3})-([0-9]{4})')
);

INSERT INTO Marca (marca) VALUES ('fiat');
SELECT * FROM Marca;
INSERT INTO Marca (marca) VALUES ('ford');

INSERT INTO Carro (modelo,placa,marca) VALUES ('siena','qwe=1234',1);

SELECT * FROM CARRO;

DROP TABLE Marca

CREATE TABLE Cliente(
	codcliente SERIAL NOT NULL,
	cpf NUMERIC(5,0) NOT NULL,
	nome VARCHAR(45) NOT NULL,
	sexo CHAR(1) NOT NULL,
	data_nascimento DATE NOT NULL,
	logradouro VARCHAR(45) NOT NULL,
	numero SMALLINT NOT NULL,
	complemento VARCHAR(30),
	bairro VARCHAR(30) NOT NULL,
	cidade VARCHAR(30),
	estado VARCHAR(15),
	cep CHAR(8),
	
	PRIMARY KEY (codcliente),
	CONSTRAINT sexo_valido CHECK(sexo in ('m','f','o')),

);

CREATE TABLE Telefone(
	codcliente INT,
	telefone INT,
	
	PRIMARY KEY (codcliente,telefone),
	FOREIGN KEY (codcliente) REFERENCES Cliente(codcliente),
	CONSTRAINT fone_nove_digitos CHECK (telefone > 99999999 AND telefone < 1000000000),
);

CREATE TABLE Marca(
	codmarca SERIAL,
	marca VARCHAR(15)
	
	PRIMARY KEY (codmarca)
);

CREATE TABLE Carro(
	codcarro SERIAL,
	modelo VARCHAR(20),
	placa CHAR(8),
	marca INT,
	
	PRIMARY KEY (codcarro),
	FOREIGN KEY (marca) REFERENCES Marca(codmarca),
	CONSTRAINT placa_valida CHECK (placa ~* '([A-Z]{3})-([0-9]{4})')
);

CREATE TABLE Funcionario(
	codfuncionario SERIAL,
	matricula VARCHAR(6),
	nome VARCHAR(45),
	
	PRIMARY KEY (codfuncionario),
	
);

CREATE TABLE Locacao(
	codlocacao SERIAL,
	codcliente INT,
	codcarro INT,
	codfuncionario INT,
	data_locacao TIMESTAMP NOT NULL,
	dia_devolucao DATE NOT NULL,
	valor MONEY NOT NULL,
	
	PRIMARY KEY (codlocacao),
	FOREIGN KEY (codcliente) REFERENCES Cliente(codcliente),
	FOREIGN KEY (codcarro) REFERENCES Cliente(codcarro),
	FOREIGN KEY (codfuncionario) REFERENCES Cliente(codfuncionario),
);