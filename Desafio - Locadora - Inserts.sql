--Cliente
INSERT INTO Cliente(cpf,nome,sexo,data_nascimento,logradouro,numero,bairro) VALUES ('11111122222','Maria Fernanda Nobrega','f','12/04/1990','R. Dom João V',' 120','Miramar');
INSERT INTO Cliente(cpf,nome,sexo,data_nascimento,logradouro,numero,bairro) VALUES ('96452997623','Natalia Campos','o','02/07/1988','R. Padre Ibiapina','56','Cabo Branco');
INSERT INTO Cliente(cpf,nome,sexo,data_nascimento,logradouro,numero,bairro) VALUES ('75432334512','Jorge Alencar','m','23/01/1993','R. Boa Esperança','89','Miramar');
INSERT INTO Cliente(cpf,nome,sexo,data_nascimento,logradouro,numero,bairro) VALUES ('09746356654','Gabriel Nascimento','o','12/04/1988','R. Dom João V','310','Miramar');
INSERT INTO Cliente(cpf,nome,sexo,data_nascimento,logradouro,numero,bairro) VALUES ('74635928374','Leticia Alencar','f','01/09/1988','R. da Paz','21','Bancários');

--Marca
INSERT INTO Marca(marca,preco) VALUES ('CHEVROLET',100);
INSERT INTO Marca(marca,preco) VALUES ('FIAT',100);
INSERT INTO Marca(marca,preco) VALUES ('KIA',80);
INSERT INTO Marca(marca,preco) VALUES ('RENAULT',80);

--Carro
INSERT INTO Carro(modelo,placa,marca) VALUES ('SPIN','ABC-1342',1);
INSERT INTO Carro(modelo,placa,marca) VALUES ('SIENA','PPG-7763',2);
INSERT INTO Carro(modelo,placa,marca) VALUES ('SOUL','AAA-5522',3);
INSERT INTO Carro(modelo,placa,marca) VALUES ('SANDERO','NAU-8332',4);

--Funcionario
INSERT INTO Funcionario(matricula,nome) VALUES ('314264','Paulo Andrade');
INSERT INTO Funcionario(matricula,nome) VALUES ('089785','Vitoria Veloso');

--Locacao
INSERT INTO Locacao(codcliente,codcarro,codfuncionario,dia_devolucao,valor) VALUES (1,2,2,'04/03/2018',200);
INSERT INTO Locacao(codcliente,codcarro,codfuncionario,dia_devolucao,valor) VALUES (4,1,2,'04/03/2018',160);
INSERT INTO Locacao(codcliente,codcarro,codfuncionario,dia_devolucao,valor) VALUES (4,3,1,'04/03/2018',160);

