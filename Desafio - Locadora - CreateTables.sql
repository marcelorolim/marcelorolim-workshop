CREATE TABLE Cliente(
	codcliente SERIAL,
	cpf CHAR(11) NOT NULL,
	nome VARCHAR(45) NOT NULL,
	sexo CHAR(1) NOT NULL,
	data_nascimento DATE NOT NULL,
	logradouro VARCHAR(45) NOT NULL,
	numero SMALLINT NOT NULL,
	complemento VARCHAR(30),
	bairro VARCHAR(30) NOT NULL,
	cidade VARCHAR(30),
	estado VARCHAR(15),
	cep CHAR(8),
	
	PRIMARY KEY (codcliente),
	CONSTRAINT sexo_valido CHECK (sexo in ('m','f','o')),
	CONSTRAINT cpf_valido CHECK (cpf ~ '([0-9]{11})'),
    CONSTRAINT cep_valido CHECK (cep ~ '([0-9]{8})')
);

CREATE TABLE Telefone(
	codcliente INT,
	telefone CHAR(9),
	
	PRIMARY KEY (codcliente,telefone),
	FOREIGN KEY (codcliente) REFERENCES Cliente(codcliente),
	CONSTRAINT fone_nove_digitos CHECK (telefone ~ '([0-9]{9})')
);

CREATE TABLE Marca(
	codmarca SERIAL,
	marca VARCHAR(15),
    preco MONEY NOT NULL,
	
	PRIMARY KEY (codmarca)
);

CREATE TABLE Carro(
	codcarro SERIAL,
	modelo VARCHAR(20),
	placa CHAR(8),
	marca INT,
	
	PRIMARY KEY (codcarro),
	FOREIGN KEY (marca) REFERENCES Marca(codmarca),
	CONSTRAINT placa_valida CHECK (placa ~* '([A-Z]{3})-([0-9]{4})')
);

CREATE TABLE Funcionario(
	codfuncionario SERIAL,
	matricula VARCHAR(6),
	nome VARCHAR(45),
	
	PRIMARY KEY (codfuncionario)
);

CREATE TABLE Locacao(
	codcliente INT,
	codcarro INT,
	codfuncionario INT,
	data_locacao TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP),
	dia_devolucao DATE NOT NULL,
	valor MONEY NOT NULL,
	
	PRIMARY KEY (codcliente,codcarro,codfuncionario,data_locacao),
	FOREIGN KEY (codcliente) REFERENCES Cliente(codcliente),
	FOREIGN KEY (codcarro) REFERENCES Carro(codcarro),
	FOREIGN KEY (codfuncionario) REFERENCES Funcionario(codfuncionario)
);



